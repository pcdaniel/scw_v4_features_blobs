%   Description: V4 Blob Extraction for IFCB Files
%   Author: P. Daniel
%   Comment: See google doc for detailed instructions and how to setup


% Add only code that is required to path
% It is important that other version of ifcb-analysis are nto added to the
% path. If so it can lead to weird bugs.
addpath("~/Documents/MATLAB/ifcb-analysis/webservice_tools/");
addpath("~/Documents/MATLAB/ifcb-analysis/Development/Heidi_explore/blobs_for_biovolume/");
addpath("~/Documents/MATLAB/ifcb-analysis/feature_extraction/");
addpath("~/Documents/MATLAB/ifcb-analysis/feature_extraction/blob_extraction/");
addpath("/home/pcdaniel/Documents/MATLAB/ifcb-analysis/IFCB_tools")

% Check if parallel pool has been started already and kill it if so.
poolobj = gcp('nocreate');  
if isempty(poolobj)
    delete(poolobj);
end

% Pull the data directly off of Synology
% The mount point is /media/lab-synology/
% Synology is not mounted, run the shell script:
% /home/pcdaniel/Documents/linux-stuff/mount_104.sh
 
base_dir = '/media/lab-synology/';

% Save the blob files onto this machine
save_dir = '/media/data-drive/scw-v4/';

% Run in Pararllel or on a single core
parallel_flag = true;

% Loop through each year 
for year = 2015:2023
    start_blob_batch([base_dir 'data/' num2str(year) '/'] , [save_dir 'blobs/' num2str(year) '/'] , parallel_flag);
end