% Run V4 Feature Extraction 
% Add Webtools directory to path
addpath("~/Documents/MATLAB/ifcb-analysis/webservice_tools/");
addpath("~/Documents/MATLAB/ifcb-analysis/Development/Heidi_explore/blobs_for_biovolume/");
addpath("~/Documents/MATLAB/ifcb-analysis/feature_extraction/biovolume/");
addpath("~/Documents/MATLAB/ifcb-analysis/feature_extraction/");
addpath("/home/pcdaniel/Documents/MATLAB/ifcb-analysis/IFCB_tools")

% Check if worker pool was started or not. If so, kill it. This will
% restart when start_features_batch is called.
poolobj = gcp('nocreate');
if isempty(poolobj)
    delete(poolobj);
end

% Pull the data directly off of Synology
% The mount point is /media/lab-synology/
% Synology is not mounted, run the shell script:
% /home/pcdaniel/Documents/linux-stuff/mount_104.sh
 
base_dir = '/media/lab-synology/';

% Save the blob files onto this machine
save_dir = '/media/data-drive/scw-v4/';

% Run in Pararllel or on a single core
parallel_flag = true;

% Loop through each year 
for year = 2015:2023
    start_feature_batch([base_dir 'data/' num2str(year) '/'] , [save_dir 'blobs/' num2str(year) '/'], [save_dir 'features/' num2str(year) '/'] , parallel_flag);
end


% Old way
% parallel_flag = true;
% start_feature_batch([base_dir 'data/2022/'] , [base_dir 'blobs/2022/'] , [base_dir 'features/2022/'], parallel_flag);