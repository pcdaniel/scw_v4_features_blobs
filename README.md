# Santa Cruz Wharf - Features & Blobs V4 #
---
The Version 4 code is located in a branch of the `ifcb-analysis` project. In order to get that code and get it to run you have to first 'clone' the project and then checkout the branch that you want to use.

This code benefits greatly from be run in parallel, so it is imporant to run with as many CPU cores as are available.

### Clone the Repo ###

```bash
git clone https://github.com/hsosik/ifcb-analysis
```

### Checkout the 'feature_v3' branch ###

Now change the branch from the "master" to "feature_v3"

```bash
git checkout features_v3
```
---
## Blob Extraction ##

Run `ifcb_104_blob_v4.m` in matlab. The way things are configured now is to run through every year 2015:2023, although it doesn't rerun the blobs is they are already run, alhtough this does add some overhead.

---
## Features Extraction ##

After blobs are extracted, the features can be extracted using `ifcb_104_feature_v4.m`

---
# Results #
Biovolume data from 2023 from the SCW was compared between the V2 and V4 versions of feature extraction. The are generally a linear fit (see red regression line below), with V2 generally overestimating Biovolume relativate to V4.

There are several cases wehere biovolumes are incosistent and they are generally for detritus, where V2 does not calculate a BV or it is a very small value, but V4 does. However, this is not consistent.

<img width=450px style="display:block; margin-left: auto; margin-right: auto;" src="./figures/scw_v4_detritus.png"></img>

### Case: V2 below 1000 (eqd~12um) &  V4 is large < 5e6 ###

Blobs and outlines do not seem to be the issues for these edgecases. Here is an example of from <a href="http://akashiwo.oceandatacenter.ucsc.edu:8000/image?image=00074&bin=D20230429T094042_IFCB104">D20230429T094042_IFCB104_00074</a>

V2 Biovolume: 177.3 $\mu m^3$  
V4 Biovolume: 2.1e+06 $\mu m^3$  

Image Outline (V2)
<img width=450px style="display:block; margin-left: auto; margin-right: auto;" src="./figures/D20230429T094042_IFCB104_74_outline.png"></img>


Here are 40 examples where the V4 had a large (reasonable) Biovolume that was completely missed by the V2. In most cases, the images are detritus or amorphous, suggesting that V4 may perform better in these edge cases.

<img width=450px style="display:block; margin-left: auto; margin-right: auto;" src="./figures/badV2_2023.png"></img>


<img width=350px style="display:block; margin-left: auto; margin-right: auto;" src="./figures/log_v2_v4_scatter_2023.png"></img>

From a timeseries perspective, trends in biovolume are consistent and variability throughout the day are consistent across versions.
<img width=450px style="display:block; margin-left: auto; margin-right: auto;" src="./figures/timeseries_box_2023.png"></img>


<img width=450px style="display:block; margin-left: auto; margin-right: auto;" src="./figures/timeseries_2023.png"></img>

<img width=450px style="display:block; margin-left: auto; margin-right: auto;" src="./figures/timeseries_differences_2023.png"></img>


Exploring biovolume by class seems to be the next step to understand where the differences arise from, from a wide view it seems that difference between the two methods are not major.